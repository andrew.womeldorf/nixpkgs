{ pkgs ? import <nixpkgs> { }
, lib
, awscli2 ? pkgs.awscli2
, jq ? pkgs.jq
, makeWrapper ? pkgs.makeWrapper
, stdenv ? pkgs.stdenv
}:

stdenv.mkDerivation rec {
  name = "aws-sso-temp-creds";

  src = ./.;

  nativeBuildInputs = [ makeWrapper ];

  installPhase = ''
    mkdir -p $out/bin
    cp aws-sso-temp-creds.sh $out/bin/aws-sso-temp-creds
    wrapProgram $out/bin/aws-sso-temp-creds \
      --prefix PATH : ${lib.makeBinPath [ awscli2 ]} \
      --prefix PATH : ${lib.makeBinPath [ jq ]}
  '';
}
