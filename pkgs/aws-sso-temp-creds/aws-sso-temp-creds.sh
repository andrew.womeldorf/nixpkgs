# Export temporary AWS Credentials after signing into an account with AWS SSO.
#
# https://aws.amazon.com/premiumsupport/knowledge-center/sso-temporary-credentials/
#
# Usage:
#   export AWS_PROFILE=<profile>
#   aws sso login
#   eval "$(aws-sso-tmp-creds)"
#
# Usage w/ direnv. This will auto-cleanup the env upon leaving as well:
#   shellHook = ''
#     TMPFILE=$(mktemp "/tmp/$(basename $0).XXXXXXX")
#     AWS_PROFILE=sec-image-prod aws-sso-temp-creds >> $TMPFILE
#     cat $TMPFILE
#     source $TMPFILE
#     rm $TMPFILE
#   ''
#
# Note: I've noticed that when using the serverless framework, it doesn't like
# the AWS_PROFILE variable being set if the profile is configured for SSO.
# Unset the variable after running this script to work with sls:
#   unset AWS_PROFILE

if [ -z "${AWS_PROFILE}" ]; then
    echo "Missing AWS_PROFILE env var"
else
    # TODO: How to get this cache path dynamically
    accessToken=$(cat ~/.aws/sso/cache/132ff1714a301ae4e28df59c521b6a1d98aac252.json | jq -r '.accessToken')
    accountID=$(sed -n "/\[profile ${AWS_PROFILE}\]/,/^$/p" ~/.aws/config | grep sso.account.id | cut -d ' ' -f 3)
    roleName=$(sed -n "/\[profile ${AWS_PROFILE}\]/,/^$/p" ~/.aws/config | grep sso.role.name | cut -d ' ' -f 3)
    region=$(sed -n "/\[profile ${AWS_PROFILE}\]/,/^$/p" ~/.aws/config | grep sso.region | cut -d ' ' -f 3)

    creds=$(aws sso get-role-credentials --account-id $accountID --role-name $roleName --access-token $accessToken --region $region)

    cat <<HERE
export AWS_ACCESS_KEY_ID=$(echo $creds | jq -r '.roleCredentials.accessKeyId')
export AWS_SECRET_ACCESS_KEY=$(echo $creds | jq -r '.roleCredentials.secretAccessKey')
export AWS_SESSION_TOKEN=$(echo $creds | jq -r '.roleCredentials.sessionToken')
HERE
fi
