# Create an AWS MFA Session with a virtual MFA token
#
# Usage:
#   aws_mfa_session -p <profile_name>
#
# Prints credentials to export to the terminal
  
# Creates an STS session and consumes assumed creds into AWS ENV vars
function aws_mfa_session() {
  err() {
    echo >&2 "ERROR: $1"
  }

  p=$AWS_PROFILE
  
  usage() {
    echo >&2 "Usage: aws_mfa_session -p {AWS_PROFILE} [-d {SESSION_DURATION_IN_SECS}] [-h]"
  }
  
  parse_opts() {
    local OPTIND
    local OPTERR
    local OPTARG
    local opt
    while getopts ':hp:d:' opt
    do
      case "${opt}" in
        h)
          usage
          return 0
          ;;
        p)
          p=$OPTARG
          echo "set p = ${p}"
          ;;
        d)
          d=$OPTARG
          if ! echo $d | grep -Esq '^[0-9]{3,5}$'
          then
            err "Invalid argument: Duration must be a 3-5 digit number: $d"
            usage
            return 3
          fi
          ;;
        \?)
          usage
          return 1
          ;;
        :)
          err "Invalid option: \"-$OPTARG\" requires an argument"
          ;;
      esac
    done
  
    if [[ "$p" == "" ]]
    then
      err "No profile specified"
      usage
      return 2
    fi
  
    shift $((OPTIND - 1))
  }
  
  parse_opts $@
  local rc=$?
  
  local profile=$p
  local duration=${d:-43200}
  
  unset p
  unset d
  
  if [[ $rc != 0 ]]
  then
    return $rc
  fi
  
  local mfa_code
  while [[ "$mfa_code" == "" ]]; do
    echo -n "Generate an MFA code and enter it: "
  
    local input
    read input
    if echo $input | grep -Esq "^[0-9]{6}$"
    then
      mfa_code=$input
    else
      err "Invalid response: \"$input\".  MFA Code must be a 6 digit string."
    fi
  done
  
  local region=$(aws --profile $profile configure get region)
  local principal=`aws --region $region --profile $profile sts get-caller-identity | grep '\"Arn\"' | awk -F\" '{print $4}'`
  local serial_no=`echo $principal | sed 's/user/mfa/'`
  
  local AWS_CREDS=$(aws --profile $profile sts get-session-token --duration-seconds $duration --serial-number $serial_no --token-code $mfa_code --output json)
  
  cat <<HERE

  export AWS_DEFAULT_REGION=$region
  export AWS_ACCESS_KEY_ID=$(echo "${AWS_CREDS}" | grep AccessKeyId | awk -F'"' '{print $4}')
  export AWS_SECRET_ACCESS_KEY=$(echo "${AWS_CREDS}" | grep SecretAccessKey | awk -F'"' '{print $4}')
  export AWS_SESSION_TOKEN=$(echo "${AWS_CREDS}" | grep SessionToken | awk -F'"' '{print $4}')
  export AWS_SECURITY_TOKEN=$(echo "${AWS_CREDS}" | grep SessionToken | awk -F'"' '{print $4}')
  export AWS_SESSION_EXPIRATION=$(echo "${AWS_CREDS}" | grep Expiration | awk -F'"' '{print $4}')

HERE
  
  echo "assumed the following identity..."
  aws sts get-caller-identity
}

aws_mfa_session "$@"
