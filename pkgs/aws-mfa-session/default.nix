{ pkgs ? import <nixpkgs> { }
, lib
, awscli2 ? pkgs.awscli2
, makeWrapper ? pkgs.makeWrapper
, stdenv ? pkgs.stdenv
}:

stdenv.mkDerivation rec {
  name = "aws_mfa_session";

  src = ./.;

  nativeBuildInputs = [ makeWrapper ];

  installPhase = ''
    mkdir -p $out/bin
    cp aws_mfa_session.sh $out/bin/aws_mfa_session
    wrapProgram $out/bin/aws_mfa_session \
      --prefix PATH : ${lib.makeBinPath [ awscli2 ]}
  '';
}
