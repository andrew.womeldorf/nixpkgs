{ pkgs
, runCommand
}:
# https://www.youtube.com/watch?v=1nU_hR2kod4&list=PLRGI9KQ3_HP_OFRG6R-p4iFgMSK1t5BHs&index=5
let
  LS_COLORS = pkgs.fetchgit {
    url = "https://github.com/trapd00r/LS_COLORS";
    rev = "d20f554612a21f18623a00a3a024e7ee225678ed";
    sha256 = "1wnzaay4r6sbqnxylpc31pwsq89km8y5f9caf0fq60lfabg139g3";
  };
  #LS_COLORS = pkgs.fetchurl {
    #url = "https://raw.githubusercontent.com/trapd00r/LS_COLORS/d20f554612a21f18623a00a3a024e7ee225678ed/LS_COLORS";
    #sha256 = "0iwldj2rcq50q9jzm2ngn739giyvcgzdjcgiwjjnm2g7y0lh0z68";
  #};
in
  pkgs.runCommand "ls-colors" {} ''
    mkdir -p $out/bin $out/share
    ln -s ${pkgs.coreutils}/bin/ls $out/bin/ls
    ln -s ${pkgs.coreutils}/bin/dircolors $out/bin/dircolors
    cp ${LS_COLORS}/LS_COLORS $out/share/LS_COLORS
  ''
