{ pkgs ? import <nixpkgs> { }
, lib ? pkgs.lib
, buildGoModule ? pkgs.buildGoModule
, fetchgit ? pkgs.fetchgit
, git ? pkgs.git
, makeWrapper ? pkgs.makeWrapper
}:

buildGoModule rec {
  pname = "colima";
  version = "0.3.0-ci-test";

  src = fetchgit {
    url = "https://github.com/abiosoft/colima.git";
    rev = "d99e306af18b4459ea1562434899756b234816d6";
    sha256 = "0dnvinfphc3pdvp20ghz9pl1xj7018j1zb0m6wfbf6p9017sdvya";
    leaveDotGit = true;
    deepClone = true;
  };

  vendorSha256 = "13zv53n9pmc0h0df1nrkrza5xr6gj7kl34671mizy15bskqbfphf";

  nativeBuildInputs = [ git makeWrapper ];

  buildPhase = ''
    runHook preBuild
    git tag v${version}
    make
    runHook postBuild
  '';

  installPhase = ''
    runHook preInstall
    mkdir -p "$out/bin"
    cp -r _output/binaries/colima-$(uname)-$(uname -m) $out/bin/colima
    runHook postInstall
  '';

  meta = with lib; {
    description = "Container runtimes on macOS (and Linux) with minimal setup.";
    homepage = "https://github.com/abiosoft/colima";
    license = licenses.mit;
    #maintainers = with maintainers; [ awomeldorf ];
    #platforms = [ "x86_64-linux" ];
  };
}
