# Colima

Container runtimes on macOS (and Linux) with minimal setup.

v0.3.0 wasn't quite out yet at the time I made this derivation. It's entirely possible it will need to change at a later date.

In order to use colima, you also need:

- docker
- lima

I hesitate to wrap colima in these. In particular, this version of colima requires lima>=0.7.4, and at the moment, it looks like 0.7.4 was only just merged into master, so until hydra finishes, I can't access it from unstable. I had to install it from local.
