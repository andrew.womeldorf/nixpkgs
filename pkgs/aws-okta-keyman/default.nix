{ lib
, nix
, buildPythonApplication
, fetchPypi
, python3Packages
}:

buildPythonApplication rec {
  pname = "aws-okta-keyman";
  version = "0.8.5";

  src = fetchPypi {
    inherit version;
    pname = "aws_okta_keyman";
    sha256 = "1k1aggwyyb11q6qgjiwibmjwllqmrim3nv86mx3isar425q4bjbx";
  };

  # `coverage` is pinned to 5.2, and I don't have enough working knowledge to
  # use a pinned python package like this in nix. tests fail as a result of a
  # version conflict, so skipping. I'm very open to suggestion.
  doCheck = false;

  propagatedBuildInputs = [
    nix
  ] ++ (with python3Packages; [
    beautifulsoup4
    boto3
    colorlog
    configparser
    future
    keyring
    requests
    pyyaml
  ]);

  meta = with lib; {
    description = "Login to Okta and generate AWS Credentials";
    longDescription = ''
      This is a simple command-line tool for logging into Okta and generating
      temporary Amazon AWS Credentials. This tool makes it easy and secure to
      generate short-lived, logged and user-attributed credentials that can be
      used for any of the Amazon SDK libraries or CLI tools.
    '';
    homepage = "https://github.com/nathan-v/aws_okta_keyman";
  };
}
