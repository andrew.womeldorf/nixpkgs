{ pkgs ? import <nixpkgs> { } }:

with pkgs;

rec {
  aws_mfa_session = callPackage ./pkgs/aws_mfa_session { };

  aws-okta-keyman = python3Packages.callPackage ./pkgs/aws-okta-keyman { };

  aws-sso-temp-creds = callPackage ./pkgs/aws-sso-temp-creds { };

  colima = callPackage ./pkgs/colima { };

  ls-colors = callPackage ./pkgs/ls-colors { };
}
