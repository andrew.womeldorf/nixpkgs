# nixpkgs

Adding nix packages for myself. Maybe someday when I'm all grown up, I will clean it up and start merging them into nixos/nixpkgs.

## Local only

Be in this directory.

`nix-env -f . -i ls-colors`

## TODO

- [ ] I want to emulate the maintainers list that `nixos/nixpkgs` does, even if it is only me, mostly for learning more about imports.
- [ ] Create a `nix-channel` out of the repo, so I can use this without cloning the repo
- [ ] How does `nixos/nixpkgs` append the `-${version}` to the package names? I want that.
- [ ] Investigate: Is there a way, while local only, to embed the location of these files so that if I inspect the package with `nix-env`, I can be reminded of where this repo is?
- [ ] How to install these with home-manager?
