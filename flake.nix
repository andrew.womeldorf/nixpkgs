{
  description = "Try making this repo a flake. Maybe it'll be easier than a channel.";

  inputs.nixpkgs.url = "github:nixos/nixpkgs";

  outputs = { self, nixpkgs }:
    let
      supportedSystems = nixpkgs.lib.platforms.darwin;
      forAllSystems = f: nixpkgs.lib.genAttrs supportedSystems (system: f system);
      nixpkgsFor = forAllSystems (system: import nixpkgs { inherit system; });
    in rec {
      packages = forAllSystems (system: {
        aws_mfa_session = nixpkgsFor.${system}.callPackage ./pkgs/aws-mfa-session { };
        aws-okta-keyman = nixpkgsFor.${system}.python3Packages.callPackage ./pkgs/aws-okta-keyman { };
        aws-sso-temp-creds = nixpkgsFor.${system}.callPackage ./pkgs/aws-sso-temp-creds { };
        colima = nixpkgsFor.${system}.callPackage ./pkgs/colima { };
        ls-colors = nixpkgsFor.${system}.callPackage ./pkgs/ls-colors { };
      });
    };
}
